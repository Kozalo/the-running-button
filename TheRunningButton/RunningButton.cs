﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace KozaloRuTheRunningButton
{
    internal sealed class RunningButton : Button
    {
        private const int Interval = 3;     // Интервал для таймера
        private const string Id = "RunningButton";
        private readonly int _widthCorrection = 19;    // Почему-то ширина и высота формы немного больше, чем на самом деле.
        private readonly int _heightCorrection = 40;   // Эти значения компенсируют различия.

        public int Vx;              // Скорости кнопки (смещение кнопки за кадр);
        public int Vy;              // оставляю открытыми по аналогии с остальными свойствами стандартных компонентов.
        public int TopPadding;      // Корректирует верхнюю границу формы при наличии меню.
        public int ChangeRange;     // Задаёт диапазон случайных значений для смены скорости.
        public bool CanIntersect;   // Определяет будут ли кнопки сталкиваться или просто пролетать мимо.
        public bool Intersects;     // Устанавливается в истину, если кнопка на текущем кадре столкнулась с другой.

        private readonly Timer _timerToRun;
        private bool _isMoving = false;
        private int _offsetX, _offsetY;     // Используется для правильного позиционирования кнопок при перетаскивании.

        public RunningButton(Control parent, int width, int height, string text, int changeRange, int x = 0, int y = 0, int vx = 0, int vy = 0, bool canIntersect = false, int topPadding = 0)
        {
            Vx = vx;
            Vy = vy;
            TopPadding = topPadding;

            Name = Id;
            Width = width;
            Height = height;
            Top = y;
            Left = x;
            Parent = parent;
            Text = text;
            Anchor = AnchorStyles.None;
            CanIntersect = canIntersect;
            ChangeRange = changeRange;
            Visible = true;
            Click += RunningButton_Click;   // Скрытое преобразование в "new EventHandler(RunningButton_Click)"?

            // Если же родитель не форма, то нет необходимости что-либо корректировать
            if (!(parent is Form))
            {
                _widthCorrection = 0;
                _heightCorrection = 0;
            }

            _timerToRun = new Timer { Interval = Interval };
            _timerToRun.Tick += timerToRun_Tick;
        }

        public void Start()
        {
            _timerToRun.Enabled = true;
        }

        public void Stop()
        {
            _timerToRun.Enabled = false;
        }

        // Этот режим отключает возможность случайно изменять скорость кнопок, кликая по ним, но зато позволяет двигать сами кнопки, перетаскивая их мышью.
        // Вызов с параметром false возвращает всё обратно.
        public void LetMoveButtonManually(bool yes = true)
        {
            if (yes)
            {
                MouseDown += RunningButton_MouseDown;
                MouseUp += RunningButton_MouseUp;
                MouseMove += RunningButton_MouseMove;
                Click -= RunningButton_Click;
            }
            else
            {
                MouseDown -= RunningButton_MouseDown;
                MouseUp -= RunningButton_MouseUp;
                MouseMove -= RunningButton_MouseMove;
                Click += RunningButton_Click;
            }
        }

        // Вызывается из кода объекта-родителя при изменении его размера.
        public void FormResizedReaction(Control parent, int padding = 0) 
        {
            if ((Left + Width + _widthCorrection) > parent.Width) Left = (parent.Width - Width - _widthCorrection);
            else if (Left < 0) Left = 0;

            if ((Top + Height + _heightCorrection) > parent.Height) Top = (parent.Height - Height - _heightCorrection);
            else if (Top < padding) Top = padding;
        }

        public void Remove()
        {
            Stop();
            Visible = false;
            _timerToRun.Dispose();
            Dispose();
        }

        public bool IntersectsWith(Control button)
        {
            return Bounds.IntersectsWith(button.Bounds);
        }

        private void timerToRun_Tick(object sender, EventArgs e)
        {
            Intersects = false;

            if (CanIntersect)
            {
                foreach (var btn in Parent.Controls.Find(Id, true))
                {
                    if (!IntersectsWith(btn) || (this == btn))
                        continue;

                    Intersects = true;
                    var intersectedParams = GetIntersectedSide(btn);

                    // Проверяются угловые столкновения и только с какой-либо стороны.
                    if ((intersectedParams.IntersectedLeftSide && intersectedParams.IntersectedTopSide) || (intersectedParams.IntersectedRightSide && intersectedParams.IntersectedTopSide) || (intersectedParams.IntersectedBottomSide && intersectedParams.IntersectedLeftSide) || (intersectedParams.IntersectedBottomSide && intersectedParams.IntersectedRightSide))
                    {
                        if (intersectedParams.XDifference > (btn.Width / 2))
                            Vy = -Vy;
                        else if (intersectedParams.YDifference > (btn.Height / 2))
                            Vx = -Vx;
                        else
                        {

                            Vx = -Vx;
                            Vy = -Vy;
                        }
                    }
                    else if (intersectedParams.IntersectedLeftSide || intersectedParams.IntersectedRightSide)
                        Vx = -Vx;
                    else if (intersectedParams.IntersectedTopSide || intersectedParams.IntersectedBottomSide)
                        Vy = -Vy;
                }
            }


            // Проверка столкновения с границами родителя либо вылета за них.
            if (((Left + Width + _widthCorrection) >= Parent.Width) || (Left <= 0))
            {
                Vx = -Vx;

                if (Left < 0) Left = 0;
                else if ((Left + Width + _widthCorrection) > Parent.Width) Left = Parent.Width - Width - _widthCorrection;
            }
            if (((Top + Height + _heightCorrection) >= Parent.Height) || (Top <= TopPadding))
            {
                Vy = -Vy;

                if (Top < TopPadding) Top = 0;
                else if ((Top + Height + _heightCorrection) > Parent.Height) Top = Parent.Height - Height - _heightCorrection;
            }

            Left += Vx;
            Top += Vy;
        }

        // Случайное изменение скорости кнопки при клике по ней
        private void RunningButton_Click(object sender, EventArgs e)
        {
            var rand = new Random((int) (DateTime.Now.Ticks));
            Vx = rand.Next(-ChangeRange, ChangeRange);
            Vy = rand.Next(-ChangeRange, ChangeRange);
        }

        // Режим с перетаскиванием
        private void RunningButton_MouseDown(object sender, MouseEventArgs e)
        {
            _isMoving = true;

            _offsetX = e.X;
            _offsetY = e.Y;
        }

        private void RunningButton_MouseUp(object sender, MouseEventArgs e)
        {
            _isMoving = false;
        }

        private void RunningButton_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_isMoving)
                return;

            Left += e.X - _offsetX;
            Top += e.Y - _offsetY;
        }


        
        /*
         * Функция getIntersectedSide().
         * Параметры: две столкнувшиеся кнопки.
         * Возвращает: структуру, описывающую в четырёх булевых переменных место столкновения и в двух целочисленных размер пересекаемой области в координатах.
         * 
         * Работает не совсем корректно, так что не помешала бы доработка и обработка всех случаев столкновений. Но меня на это уже не хватает. Извините.
        */

        private struct IntersectedSideParams
        {
            public bool IntersectedTopSide;
            public bool IntersectedBottomSide;
            public bool IntersectedLeftSide;
            public bool IntersectedRightSide;

            public int XDifference;
            public int YDifference;
        }

        
        private IntersectedSideParams GetIntersectedSide(Control intersectedBtn)
        {
            var intersectedRect = Rectangle.Intersect(Bounds, intersectedBtn.Bounds);
            var sideParams = new IntersectedSideParams();

            // Сравнения работают на разницах координат сторон текущего объекта со сторонами пересекаемой области.
            // Примерно способ описан в прилагаемом к коду рисунке. Но, как говорилось выше, так обрабатываются не все варианты.

            if ((Width > intersectedBtn.Width) && (intersectedRect.Width == intersectedBtn.Width))
            {
               if (Top < intersectedRect.Top)
                   sideParams.IntersectedTopSide = true;
               else
                   sideParams.IntersectedBottomSide = true;
            }
            else if ((Height > intersectedBtn.Height) && (intersectedRect.Height == intersectedBtn.Height))
            {
               if (Left < intersectedRect.Left)
                   sideParams.IntersectedLeftSide = true;
               else
                   sideParams.IntersectedRightSide = true;
               }
               else
               {
                    if ((Left < intersectedRect.Left) && (Top < intersectedRect.Top))
                    {
                        sideParams.IntersectedTopSide = true;
                        sideParams.IntersectedLeftSide = true;
                    }
                    else if ((Left < intersectedRect.Left) && ((Top + Height) > (intersectedRect.Top + intersectedRect.Height)))
                    {
                        sideParams.IntersectedLeftSide = true;
                        sideParams.IntersectedBottomSide = true;
                    }
                    else if (((intersectedRect.Left + intersectedRect.Width) < (Left + Width)) && (Top < intersectedRect.Top))
                    {
                        sideParams.IntersectedTopSide = true;
                        sideParams.IntersectedRightSide = true;
                    }
                    else if (((intersectedRect.Left + intersectedRect.Width) < (Left + Width)) && ((Top + Height) > (intersectedRect.Top + intersectedRect.Height)))
                    {
                        sideParams.IntersectedRightSide = true;
                        sideParams.IntersectedBottomSide = true;
                    }
                    else if ((Left < intersectedRect.Left) && (Top == intersectedRect.Top) && (Height == intersectedRect.Height))
                        sideParams.IntersectedLeftSide = true;
                    else if (((intersectedRect.Left + intersectedRect.Width) < (Left + Width)) && (Top == intersectedRect.Top) && (Height == intersectedRect.Height))
                        sideParams.IntersectedRightSide = true;
                    else if ((Top < intersectedRect.Top) && (Left == intersectedRect.Left) && (Width == intersectedRect.Width))
                        sideParams.IntersectedTopSide = true;
                    else if (((intersectedRect.Top + intersectedRect.Height) < (Top + Height)) && (Left == intersectedRect.Left) && (Width == intersectedRect.Width))
                        sideParams.IntersectedBottomSide = true;
            }

            sideParams.XDifference = intersectedRect.Width;
            sideParams.YDifference = intersectedRect.Height;
            return sideParams;
        }

        // Конец функции
    }
}
