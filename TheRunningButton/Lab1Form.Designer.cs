﻿namespace KozaloRuTheRunningButton
{
    partial class Lab1Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Lab1Form));
            this.lblMyLink = new System.Windows.Forms.LinkLabel();
            this.tpCoordsTable = new System.Windows.Forms.TabPage();
            this.dgvCoordsTable = new System.Windows.Forms.DataGridView();
            this.cmDatagridContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.clearTheTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblCoordsTableAboutBlank = new System.Windows.Forms.Label();
            this.tpDynamicPage = new System.Windows.Forms.TabPage();
            this.lblDynamicAboutBlank = new System.Windows.Forms.Label();
            this.tcTabs = new System.Windows.Forms.TabControl();
            this.pControlPanel = new System.Windows.Forms.Panel();
            this.cbLetMovingWithMouse = new System.Windows.Forms.CheckBox();
            this.btnSaveFile = new System.Windows.Forms.Button();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.btnForward = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnPausePlay = new System.Windows.Forms.Button();
            this.cbIntersect = new System.Windows.Forms.CheckBox();
            this.lblAmountOfButtons = new System.Windows.Forms.Label();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.numAmountOfButtons = new System.Windows.Forms.NumericUpDown();
            this.tAddFrameToTable = new System.Windows.Forms.Timer(this.components);
            this.ttHints = new System.Windows.Forms.ToolTip(this.components);
            this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
            this.dlgSaveFile = new System.Windows.Forms.SaveFileDialog();
            this.ssStatusInfo = new System.Windows.Forms.StatusStrip();
            this.tsStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tpCoordsTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCoordsTable)).BeginInit();
            this.cmDatagridContextMenu.SuspendLayout();
            this.tpDynamicPage.SuspendLayout();
            this.tcTabs.SuspendLayout();
            this.pControlPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAmountOfButtons)).BeginInit();
            this.ssStatusInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblMyLink
            // 
            this.lblMyLink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMyLink.AutoSize = true;
            this.lblMyLink.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblMyLink.Location = new System.Drawing.Point(625, 4);
            this.lblMyLink.Name = "lblMyLink";
            this.lblMyLink.Size = new System.Drawing.Size(56, 17);
            this.lblMyLink.TabIndex = 9;
            this.lblMyLink.TabStop = true;
            this.lblMyLink.Text = "Kozalo.Ru";
            this.ttHints.SetToolTip(this.lblMyLink, "Открыть сайт автора...");
            this.lblMyLink.UseCompatibleTextRendering = true;
            this.lblMyLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.MyLinkLabel_LinkClicked);
            // 
            // tpCoordsTable
            // 
            this.tpCoordsTable.BackColor = System.Drawing.SystemColors.Control;
            this.tpCoordsTable.Controls.Add(this.dgvCoordsTable);
            this.tpCoordsTable.Controls.Add(this.lblCoordsTableAboutBlank);
            this.tpCoordsTable.Location = new System.Drawing.Point(4, 22);
            this.tpCoordsTable.Name = "tpCoordsTable";
            this.tpCoordsTable.Padding = new System.Windows.Forms.Padding(3);
            this.tpCoordsTable.Size = new System.Drawing.Size(683, 309);
            this.tpCoordsTable.TabIndex = 1;
            this.tpCoordsTable.Text = "Статистика";
            // 
            // dgvCoordsTable
            // 
            this.dgvCoordsTable.AllowUserToAddRows = false;
            this.dgvCoordsTable.AllowUserToDeleteRows = false;
            this.dgvCoordsTable.AllowUserToResizeColumns = false;
            this.dgvCoordsTable.AllowUserToResizeRows = false;
            this.dgvCoordsTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCoordsTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvCoordsTable.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvCoordsTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvCoordsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvCoordsTable.ContextMenuStrip = this.cmDatagridContextMenu;
            this.dgvCoordsTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvCoordsTable.Location = new System.Drawing.Point(0, 0);
            this.dgvCoordsTable.MultiSelect = false;
            this.dgvCoordsTable.Name = "dgvCoordsTable";
            this.dgvCoordsTable.ReadOnly = true;
            this.dgvCoordsTable.RowHeadersVisible = false;
            this.dgvCoordsTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvCoordsTable.RowTemplate.ReadOnly = true;
            this.dgvCoordsTable.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCoordsTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCoordsTable.Size = new System.Drawing.Size(683, 311);
            this.dgvCoordsTable.TabIndex = 0;
            this.dgvCoordsTable.Visible = false;
            this.dgvCoordsTable.DoubleClick += new System.EventHandler(this.dgvCoordsTable_DoubleClick_Adapter);
            // 
            // cmDatagridContextMenu
            // 
            this.cmDatagridContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearTheTableToolStripMenuItem});
            this.cmDatagridContextMenu.Name = "cmDatagridContextMenu";
            this.cmDatagridContextMenu.Size = new System.Drawing.Size(175, 26);
            // 
            // clearTheTableToolStripMenuItem
            // 
            this.clearTheTableToolStripMenuItem.Name = "clearTheTableToolStripMenuItem";
            this.clearTheTableToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.clearTheTableToolStripMenuItem.Text = "Очистить таблицу";
            this.clearTheTableToolStripMenuItem.Click += new System.EventHandler(this.clearTheTableToolStripMenuItem_Click);
            // 
            // lblCoordsTableAboutBlank
            // 
            this.lblCoordsTableAboutBlank.AutoSize = true;
            this.lblCoordsTableAboutBlank.Location = new System.Drawing.Point(243, 49);
            this.lblCoordsTableAboutBlank.Name = "lblCoordsTableAboutBlank";
            this.lblCoordsTableAboutBlank.Size = new System.Drawing.Size(96, 17);
            this.lblCoordsTableAboutBlank.TabIndex = 1;
            this.lblCoordsTableAboutBlank.Text = "Пока здесь пусто";
            this.lblCoordsTableAboutBlank.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblCoordsTableAboutBlank.UseCompatibleTextRendering = true;
            // 
            // tpDynamicPage
            // 
            this.tpDynamicPage.BackColor = System.Drawing.SystemColors.Control;
            this.tpDynamicPage.Controls.Add(this.lblDynamicAboutBlank);
            this.tpDynamicPage.Location = new System.Drawing.Point(4, 22);
            this.tpDynamicPage.Name = "tpDynamicPage";
            this.tpDynamicPage.Padding = new System.Windows.Forms.Padding(3);
            this.tpDynamicPage.Size = new System.Drawing.Size(683, 309);
            this.tpDynamicPage.TabIndex = 0;
            this.tpDynamicPage.Text = "Динамика";
            // 
            // lblDynamicAboutBlank
            // 
            this.lblDynamicAboutBlank.AutoSize = true;
            this.lblDynamicAboutBlank.Location = new System.Drawing.Point(21, 28);
            this.lblDynamicAboutBlank.Name = "lblDynamicAboutBlank";
            this.lblDynamicAboutBlank.Size = new System.Drawing.Size(518, 104);
            this.lblDynamicAboutBlank.TabIndex = 2;
            this.lblDynamicAboutBlank.Text = resources.GetString("lblDynamicAboutBlank.Text");
            this.lblDynamicAboutBlank.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDynamicAboutBlank.UseCompatibleTextRendering = true;
            // 
            // tcTabs
            // 
            this.tcTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcTabs.Controls.Add(this.tpDynamicPage);
            this.tcTabs.Controls.Add(this.tpCoordsTable);
            this.tcTabs.Location = new System.Drawing.Point(-3, 58);
            this.tcTabs.Name = "tcTabs";
            this.tcTabs.SelectedIndex = 0;
            this.tcTabs.Size = new System.Drawing.Size(691, 335);
            this.tcTabs.TabIndex = 10;
            this.tcTabs.SelectedIndexChanged += new System.EventHandler(this.tcTabs_SelectedIndexChanged);
            // 
            // pControlPanel
            // 
            this.pControlPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pControlPanel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pControlPanel.Controls.Add(this.cbLetMovingWithMouse);
            this.pControlPanel.Controls.Add(this.btnSaveFile);
            this.pControlPanel.Controls.Add(this.btnOpenFile);
            this.pControlPanel.Controls.Add(this.btnForward);
            this.pControlPanel.Controls.Add(this.lblMyLink);
            this.pControlPanel.Controls.Add(this.btnBack);
            this.pControlPanel.Controls.Add(this.btnPausePlay);
            this.pControlPanel.Controls.Add(this.cbIntersect);
            this.pControlPanel.Controls.Add(this.lblAmountOfButtons);
            this.pControlPanel.Controls.Add(this.btnStartStop);
            this.pControlPanel.Controls.Add(this.numAmountOfButtons);
            this.pControlPanel.Location = new System.Drawing.Point(0, 0);
            this.pControlPanel.Name = "pControlPanel";
            this.pControlPanel.Size = new System.Drawing.Size(685, 59);
            this.pControlPanel.TabIndex = 8;
            // 
            // cbLetMovingWithMouse
            // 
            this.cbLetMovingWithMouse.AutoSize = true;
            this.cbLetMovingWithMouse.Location = new System.Drawing.Point(154, 36);
            this.cbLetMovingWithMouse.Name = "cbLetMovingWithMouse";
            this.cbLetMovingWithMouse.Size = new System.Drawing.Size(150, 17);
            this.cbLetMovingWithMouse.TabIndex = 8;
            this.cbLetMovingWithMouse.Text = "Перенос кнопок мышью";
            this.cbLetMovingWithMouse.UseVisualStyleBackColor = true;
            this.cbLetMovingWithMouse.CheckedChanged += new System.EventHandler(this.cbLetMovingWithMouse_CheckedChanged);
            // 
            // btnSaveFile
            // 
            this.btnSaveFile.BackgroundImage = global::KozaloRuTheRunningButton.Properties.Resources.save;
            this.btnSaveFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnSaveFile.Enabled = false;
            this.btnSaveFile.Location = new System.Drawing.Point(315, 6);
            this.btnSaveFile.Name = "btnSaveFile";
            this.btnSaveFile.Size = new System.Drawing.Size(30, 24);
            this.btnSaveFile.TabIndex = 4;
            this.ttHints.SetToolTip(this.btnSaveFile, "Сохранить таблицу...");
            this.btnSaveFile.UseVisualStyleBackColor = true;
            this.btnSaveFile.Click += new System.EventHandler(this.btnSaveFile_Click);
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.BackgroundImage = global::KozaloRuTheRunningButton.Properties.Resources.open;
            this.btnOpenFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnOpenFile.Location = new System.Drawing.Point(279, 6);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(30, 24);
            this.btnOpenFile.TabIndex = 3;
            this.ttHints.SetToolTip(this.btnOpenFile, "Открыть таблицу...");
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // btnForward
            // 
            this.btnForward.BackgroundImage = global::KozaloRuTheRunningButton.Properties.Resources.forward;
            this.btnForward.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnForward.Enabled = false;
            this.btnForward.Location = new System.Drawing.Point(417, 6);
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(30, 24);
            this.btnForward.TabIndex = 6;
            this.ttHints.SetToolTip(this.btnForward, "Следующий кадр");
            this.btnForward.UseCompatibleTextRendering = true;
            this.btnForward.UseVisualStyleBackColor = true;
            this.btnForward.Click += new System.EventHandler(this.btnBackForward_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackgroundImage = global::KozaloRuTheRunningButton.Properties.Resources.back;
            this.btnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnBack.Enabled = false;
            this.btnBack.Location = new System.Drawing.Point(381, 6);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(30, 24);
            this.btnBack.TabIndex = 5;
            this.ttHints.SetToolTip(this.btnBack, "Предыдущий кадр");
            this.btnBack.UseCompatibleTextRendering = true;
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBackForward_Click);
            // 
            // btnPausePlay
            // 
            this.btnPausePlay.BackgroundImage = global::KozaloRuTheRunningButton.Properties.Resources.pauseBtn;
            this.btnPausePlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnPausePlay.Enabled = false;
            this.btnPausePlay.Location = new System.Drawing.Point(220, 6);
            this.btnPausePlay.Name = "btnPausePlay";
            this.btnPausePlay.Size = new System.Drawing.Size(24, 24);
            this.btnPausePlay.TabIndex = 2;
            this.ttHints.SetToolTip(this.btnPausePlay, "Пауза");
            this.btnPausePlay.UseVisualStyleBackColor = true;
            this.btnPausePlay.Click += new System.EventHandler(this.btnPausePlay_Click);
            // 
            // cbIntersect
            // 
            this.cbIntersect.AutoSize = true;
            this.cbIntersect.Location = new System.Drawing.Point(10, 36);
            this.cbIntersect.Name = "cbIntersect";
            this.cbIntersect.Size = new System.Drawing.Size(137, 17);
            this.cbIntersect.TabIndex = 7;
            this.cbIntersect.Text = "Столкновения кнопок";
            this.cbIntersect.UseVisualStyleBackColor = true;
            this.cbIntersect.CheckedChanged += new System.EventHandler(this.cbIntersect_CheckedChanged);
            // 
            // lblAmountOfButtons
            // 
            this.lblAmountOfButtons.AutoSize = true;
            this.lblAmountOfButtons.Location = new System.Drawing.Point(7, 12);
            this.lblAmountOfButtons.Name = "lblAmountOfButtons";
            this.lblAmountOfButtons.Size = new System.Drawing.Size(83, 17);
            this.lblAmountOfButtons.TabIndex = 1;
            this.lblAmountOfButtons.Text = "Кол-во кнопок:";
            this.ttHints.SetToolTip(this.lblAmountOfButtons, "Количество кнопок");
            this.lblAmountOfButtons.UseCompatibleTextRendering = true;
            // 
            // btnStartStop
            // 
            this.btnStartStop.Location = new System.Drawing.Point(151, 6);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(62, 24);
            this.btnStartStop.TabIndex = 1;
            this.btnStartStop.Text = "Старт";
            this.btnStartStop.UseCompatibleTextRendering = true;
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
            // 
            // numAmountOfButtons
            // 
            this.numAmountOfButtons.Location = new System.Drawing.Point(94, 9);
            this.numAmountOfButtons.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numAmountOfButtons.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numAmountOfButtons.Name = "numAmountOfButtons";
            this.numAmountOfButtons.Size = new System.Drawing.Size(49, 20);
            this.numAmountOfButtons.TabIndex = 0;
            this.numAmountOfButtons.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tAddFrameToTable
            // 
            this.tAddFrameToTable.Interval = 3;
            this.tAddFrameToTable.Tick += new System.EventHandler(this.tAddFrameToTable_Tick);
            // 
            // dlgOpenFile
            // 
            this.dlgOpenFile.Filter = "XML-файл (*.xml)|*.xml|Книга Excel (*.xls, *.xlsx)|*.xls;*.xlsx";
            this.dlgOpenFile.RestoreDirectory = true;
            this.dlgOpenFile.ShowReadOnly = true;
            this.dlgOpenFile.Title = "Открыть таблицу";
            // 
            // dlgSaveFile
            // 
            this.dlgSaveFile.FileName = "Безымянная таблица координат";
            this.dlgSaveFile.Filter = "XML-файл (*.xml)|*.xml|Книга Excel (*.xls, *.xlsx)|*.xls;*.xlsx";
            this.dlgSaveFile.RestoreDirectory = true;
            this.dlgSaveFile.Title = "Сохранить таблицу";
            // 
            // ssStatusInfo
            // 
            this.ssStatusInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsStatusLabel});
            this.ssStatusInfo.Location = new System.Drawing.Point(0, 390);
            this.ssStatusInfo.Name = "ssStatusInfo";
            this.ssStatusInfo.Size = new System.Drawing.Size(684, 22);
            this.ssStatusInfo.TabIndex = 11;
            // 
            // tsStatusLabel
            // 
            this.tsStatusLabel.Name = "tsStatusLabel";
            this.tsStatusLabel.Size = new System.Drawing.Size(45, 17);
            this.tsStatusLabel.Text = "Готово";
            // 
            // Lab1Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 412);
            this.Controls.Add(this.ssStatusInfo);
            this.Controls.Add(this.pControlPanel);
            this.Controls.Add(this.tcTabs);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(600, 300);
            this.Name = "Lab1Form";
            this.Text = "Бегающие кнопки";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Lab1Form_FormClosing);
            this.Shown += new System.EventHandler(this.Lab1Form_Shown);
            this.Resize += new System.EventHandler(this.Lab1Form_Resize);
            this.tpCoordsTable.ResumeLayout(false);
            this.tpCoordsTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCoordsTable)).EndInit();
            this.cmDatagridContextMenu.ResumeLayout(false);
            this.tpDynamicPage.ResumeLayout(false);
            this.tpDynamicPage.PerformLayout();
            this.tcTabs.ResumeLayout(false);
            this.pControlPanel.ResumeLayout(false);
            this.pControlPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAmountOfButtons)).EndInit();
            this.ssStatusInfo.ResumeLayout(false);
            this.ssStatusInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel lblMyLink;
        private System.Windows.Forms.TabPage tpCoordsTable;
        private System.Windows.Forms.TabControl tcTabs;
        private System.Windows.Forms.TabPage tpDynamicPage;
        private System.Windows.Forms.Panel pControlPanel;
        private System.Windows.Forms.Label lblAmountOfButtons;
        private System.Windows.Forms.NumericUpDown numAmountOfButtons;
        private System.Windows.Forms.Button btnPausePlay;
        private System.Windows.Forms.CheckBox cbIntersect;
        private System.Windows.Forms.Button btnStartStop;
        private System.Windows.Forms.Button btnForward;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.DataGridView dgvCoordsTable;
        private System.Windows.Forms.Label lblCoordsTableAboutBlank;
        private System.Windows.Forms.Button btnSaveFile;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Label lblDynamicAboutBlank;
        private System.Windows.Forms.CheckBox cbLetMovingWithMouse;
        private System.Windows.Forms.Timer tAddFrameToTable;
        private System.Windows.Forms.ToolTip ttHints;
        private System.Windows.Forms.OpenFileDialog dlgOpenFile;
        private System.Windows.Forms.SaveFileDialog dlgSaveFile;
        private System.Windows.Forms.StatusStrip ssStatusInfo;
        private System.Windows.Forms.ToolStripStatusLabel tsStatusLabel;
        private System.Windows.Forms.ContextMenuStrip cmDatagridContextMenu;
        private System.Windows.Forms.ToolStripMenuItem clearTheTableToolStripMenuItem;
    }
}

