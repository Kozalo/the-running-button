﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using KozaloRuTheRunningButton.Properties;

namespace KozaloRuTheRunningButton
{
    public partial class Lab1Form : Form
    {
        public const int VBase = 3;    // Скорость кнопок.
        private const string MyUrl = "http://kozalo.ru";

        private readonly List<RunningButton> _buttonsCollection = new List<RunningButton>();
        private bool _isWorking = false;
        private bool _isPaused = false;
        private bool _timelapseFlag = false;      // Флаг, использующийся при покадровом перемещении.
        private bool _deletedFlag = false;        // Флаг, использующийся при удалении последующих кадров с целью перезаписи.
        private bool _clearTableFlag = false;     // Флаг, использующийся в функции очистки таблицы.
        private int _selectedFrameIndex = -1;
        private int _rowsCorrection;

        public Lab1Form()
        {
            InitializeComponent();
        }

        private void Lab1Form_Resize(object sender, EventArgs e)
        {
            foreach (var btn in _buttonsCollection)
                btn.FormResizedReaction(tpDynamicPage);

            if (lblDynamicAboutBlank.Visible || lblCoordsTableAboutBlank.Visible)
                AlignBlankLabels();
        }

        private void MyLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(MyUrl);
        }

        private void btnStartStop_Click(object sender, EventArgs e)
        {
            if (lblDynamicAboutBlank.Visible || lblCoordsTableAboutBlank.Visible)
            {
                lblDynamicAboutBlank.Visible = false;
                lblCoordsTableAboutBlank.Visible = false;
                dgvCoordsTable.Visible = true;

                tcTabs.SelectedIndexChanged -= tcTabs_SelectedIndexChanged;
            }


            if (_isWorking)
            {
                tAddFrameToTable.Enabled = false;

                foreach (var btn in _buttonsCollection)
                    btn.Remove();
                _buttonsCollection.Clear();

                _isWorking = false;
                _isPaused = false;
                ((Button) sender).Text = Resources.Start;
                btnPausePlay.BackgroundImage = Resources.pauseBtn;
                btnPausePlay.Enabled = false;
                numAmountOfButtons.ReadOnly = false;
                numAmountOfButtons.Increment = 1;
                _selectedFrameIndex = -1;
                btnBack.Enabled = false;
                btnForward.Enabled = false;
                tsStatusLabel.Text = Resources.Done;

                // Если кнопка нажата программно, то нужно перейти на одну из функций.
                if (_timelapseFlag)
                {
                    _timelapseFlag = false;
                    CreateButtonsFromTable();
                }

                if (_clearTableFlag)
                {
                    _clearTableFlag = false;
                    ClearTheDatagrid();
                }
            }
            else
            {
                _isWorking = true;
                ((Button) sender).Text = Resources.Stop;
                btnPausePlay.Enabled = true;
                numAmountOfButtons.ReadOnly = true;
                numAmountOfButtons.Increment = 0;
                if (!btnSaveFile.Enabled) btnSaveFile.Enabled = true;
                tsStatusLabel.Text = Resources.Processing;

                // Создание столбцов таблицы
                if (dgvCoordsTable.ColumnCount == 0)
                {
                    dgvCoordsTable.ColumnCount++;
                    dgvCoordsTable.Columns[0].Name = Resources.CanIntersect;
                    dgvCoordsTable.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                }

                while (dgvCoordsTable.ColumnCount < Math.Floor(numAmountOfButtons.Value * 5))
                {
                    dgvCoordsTable.ColumnCount += 5;

                    var strCounter = Math.Floor(dgvCoordsTable.ColumnCount / (double) 5).ToString(CultureInfo.CurrentCulture);
                    dgvCoordsTable.Columns[dgvCoordsTable.ColumnCount - 5].Name = "x" + strCounter;
                    dgvCoordsTable.Columns[dgvCoordsTable.ColumnCount - 4].Name = "y" + strCounter;
                    dgvCoordsTable.Columns[dgvCoordsTable.ColumnCount - 3].Name = "Vx" + strCounter;
                    dgvCoordsTable.Columns[dgvCoordsTable.ColumnCount - 2].Name = "Vy" + strCounter;
                    dgvCoordsTable.Columns[dgvCoordsTable.ColumnCount - 1].Name = $"{Resources.Intersects} {strCounter}";

                    dgvCoordsTable.Columns[dgvCoordsTable.ColumnCount - 5].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgvCoordsTable.Columns[dgvCoordsTable.ColumnCount - 4].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgvCoordsTable.Columns[dgvCoordsTable.ColumnCount - 3].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgvCoordsTable.Columns[dgvCoordsTable.ColumnCount - 2].SortMode = DataGridViewColumnSortMode.NotSortable;
                    dgvCoordsTable.Columns[dgvCoordsTable.ColumnCount - 1].SortMode = DataGridViewColumnSortMode.NotSortable;
                }

                var rand = new Random((int) DateTime.Now.Ticks);
                for (var i = 1; i <= numAmountOfButtons.Value; i++)
                {
                    RunningButton btn = null;
                    var fuse = 100;
                    do
                    {
                        btn?.Dispose();
                        fuse--;

                        btn = new RunningButton(tpDynamicPage, 100, 30, Resources.ButtonN + i, VBase,
                            rand.Next(Width - 119), rand.Next(Height - 70), rand.Next(-VBase, VBase),
                            rand.Next(-VBase, VBase), cbIntersect.Checked);
                        if (cbLetMovingWithMouse.Checked) btn.LetMoveButtonManually();
                    } while (_buttonsCollection.Any(button => button.IntersectsWith(btn) && fuse > 0));

                    _buttonsCollection.Add(btn);
                }

                tsStatusLabel.Text = Resources.Working;

                // Переход, если кнопка нажата программно.
                if (_timelapseFlag)
                {
                    _timelapseFlag = false;
                    LoadValuesToButtons();
                }
                else
                {
                    _buttonsCollection.ForEach(button => button.Start());
                    tAddFrameToTable.Enabled = true;    
                }
            }
        }

        private void Lab1Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (var btn in _buttonsCollection)
                btn.Remove();
        }

        private void cbIntersect_CheckedChanged(object sender, EventArgs e)
        {
            foreach (var btn in _buttonsCollection)
                btn.CanIntersect = ((CheckBox) sender).Checked;
        }

        private void btnPausePlay_Click(object sender, EventArgs e)
        {
            if (_isPaused)
            {
                tsStatusLabel.Text = Resources.Processing;
                Update();   // Принудительно обновляем экран, иначе сообщение не обновится.

                // Если продолжено с выделенного двойным кликом кадра, копируем нужные данные в списки, очищаем DataGridView и восстанавливаем данные.
                if (_selectedFrameIndex >= 0)
                {
                    var rows = new List<List<object>>();
                    for (var i = 0; i < _selectedFrameIndex; i++)
                    {
                        var cells = new List<object>();
                        for (var j = 0; j < dgvCoordsTable.Rows[i].Cells.Count; j++)
                            cells.Add(dgvCoordsTable.Rows[i].Cells[j].Value);
                         
                        rows.Add(cells);
                    }

                    dgvCoordsTable.Rows.Clear();
                    foreach (var t in rows)
                        dgvCoordsTable.Rows.Add(t.ToArray());

                    _rowsCorrection = rows.Count;
                    _selectedFrameIndex = -1;
                    _deletedFlag = true;     // А после создания нужно вернуть выделенную область на вновь созданный кадр, являющийся копией удалённого.
                }

                foreach (var btn in _buttonsCollection)
                    btn.Start();

                tAddFrameToTable.Enabled = true;
                _isPaused = false;
                ((Button) sender).BackgroundImage = Resources.pauseBtn;
                ttHints.SetToolTip(((Button) sender), Resources.Pause);
                tsStatusLabel.Text = Resources.Working;

                if (_timelapseFlag)
                    throw new Exception("PausePlay error: unexpected timelapseFlag when isPaused equals true.");
            }
            else
            {
                foreach (var btn in _buttonsCollection)
                    btn.Stop();

                tAddFrameToTable.Enabled = false;
                _isPaused = true;
                ((Button) sender).BackgroundImage = Resources.playBtn;
                ttHints.SetToolTip(((Button) sender), Resources.Continue);

                // Переход, если кнопка нажата программно
                if (_timelapseFlag)
                {
                    _timelapseFlag = false;
                    LoadValuesToButtons();
                }

                tsStatusLabel.Text = Resources.Done;
            }
        }

        private void cbLetMovingWithMouse_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox) sender).Checked)
            {
                foreach (var btn in _buttonsCollection)
                    btn.LetMoveButtonManually();
            }
            else
            {
                foreach (var btn in _buttonsCollection)
                    btn.LetMoveButtonManually(false);
            }
        }

        private void tAddFrameToTable_Tick(object sender, EventArgs e)
        {
            // Спасибо kic.a. Его совет добавлять сразу строкой из промежуточного массива помог избавиться от жутких тормозов.

            var rowValues = new List<object> { cbIntersect.Checked };

            foreach (var btn in _buttonsCollection)
            {
                rowValues.Add(btn.Left);
                rowValues.Add(btn.Top);
                rowValues.Add(btn.Vx);
                rowValues.Add(btn.Vy);
                rowValues.Add(btn.Intersects);
            }
            dgvCoordsTable.Rows.Add(rowValues.ToArray());

            // Возвращаем выделение на удалённый элемент.
            if (_deletedFlag)
            {
                _deletedFlag = false;
                dgvCoordsTable.Rows[dgvCoordsTable.SelectedRows[0].Index + _rowsCorrection].Selected = true;
            }
        }

        // Так как у DataGridView'а нет какого-нибудь PerformDoubleClick(), реализую адаптер, а программно буду вызывать другую процедуру.
        private void dgvCoordsTable_DoubleClick_Adapter(object sender, EventArgs e)
        {
            dgvCoordsTable_DoubleClick();
        }

        private void dgvCoordsTable_DoubleClick()
        {
            // Если программа в работе, то сначала остановим её, используя уже написанный код.
            // Хотя программное кликанье по кнопке может и не самый изящный вариант :)
            if (_isWorking)
            {
                _timelapseFlag = true;
                btnStartStop.PerformClick();
            }
            else CreateButtonsFromTable();
        }

        private void CreateButtonsFromTable()
        {
            // Создаём кнопки, игнорируя пустые ячейки
            var cellsCounter = dgvCoordsTable.SelectedRows[0].Cells.Cast<DataGridViewCell>().Count(cell => cell.Value != null);
            var buttonsInTable = (int) Math.Floor(--cellsCounter / (double) 5);

            numAmountOfButtons.Value = buttonsInTable;
            _timelapseFlag = true;
            btnStartStop.PerformClick();
        }

        private void LoadValuesToButtons()
        {
            _selectedFrameIndex = dgvCoordsTable.SelectedRows[0].Index;

            // Если нет паузы, то делаем её.
            if (!_isPaused)
            {
                _timelapseFlag = true;
                btnPausePlay.PerformClick();
                return;
            }

            var i = 0;
            cbIntersect.Checked = (bool) dgvCoordsTable.SelectedRows[0].Cells[i++].Value;

            foreach (var btn in _buttonsCollection)
            {
                btn.Left = (int) dgvCoordsTable.SelectedRows[0].Cells[i++].Value;
                btn.Top = (int) dgvCoordsTable.SelectedRows[0].Cells[i++].Value;
                btn.Vx = (int) dgvCoordsTable.SelectedRows[0].Cells[i++].Value;
                btn.Vy = (int) dgvCoordsTable.SelectedRows[0].Cells[i++].Value;
                btn.Intersects = (bool) dgvCoordsTable.SelectedRows[0].Cells[i++].Value;
            }

            if (!btnBack.Enabled || !btnForward.Enabled)
            {
                btnBack.Enabled = true;
                btnForward.Enabled = true;
            }
        }

        // Стрелки перемещения по кадрам всего лишь смещают выделение и "дважды кликают" по таблице.
        private void btnBackForward_Click(object sender, EventArgs e)
        {
            var i = (sender == btnBack) ? -1 : (sender == btnForward) ? 1 : 0;

            if (((dgvCoordsTable.SelectedRows[0].Index == 0) && (i == -1)) || ((dgvCoordsTable.SelectedRows[0].Index == (dgvCoordsTable.Rows.Count-1)) && (i == 1)))
                return;

            dgvCoordsTable.Rows[dgvCoordsTable.SelectedRows[0].Index + i].Selected = true;
            dgvCoordsTable_DoubleClick();
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            if (dlgOpenFile.ShowDialog() != DialogResult.OK)
                return;

            tsStatusLabel.Text = Resources.LoadingFile;
            Cursor.Current = Cursors.WaitCursor;            // UseWaitCursor не работает в подвешенном состоянии.
            clearTheTableToolStripMenuItem.PerformClick();

            if (Path.GetExtension(dlgOpenFile.FileName) == ".xml")
            {
                var xmlFile = new XmlDocument();
                xmlFile.Load(dlgOpenFile.FileName);

                var mainNode = xmlFile.SelectNodes("Lab1")?[0];
                var frames = mainNode?.SelectNodes("Frames")?[0];
                var frameList = frames?.SelectNodes("Frame");
                if (frames == null || frameList == null)
                {
                    MessageBox.Show(this, Resources.WrongFileFormat, Resources.XmlFileProcessingError,
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                foreach (XmlNode frame in frameList)
                {
                    var rowValues = new List<object>();

                    if (dgvCoordsTable.ColumnCount < 1)
                    {
                        dgvCoordsTable.ColumnCount++;
                        dgvCoordsTable.Columns[0].Name = Resources.CanIntersect;
                        dgvCoordsTable.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                    }

                    rowValues.Add(bool.Parse(frame.Attributes?[0].Value ?? "False"));
                    var buttons = frame.SelectNodes("Buttons")?[0];
                    var buttonList = buttons?.SelectNodes("Button");
                    if (buttonList == null)
                    {
                        MessageBox.Show(this, Resources.WrongFileFormat, Resources.XmlFileProcessingError,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    foreach (XmlNode button in buttonList)
                    {
                        if (button.Attributes == null || button.Attributes.Count < 5)
                        {
                            MessageBox.Show(this, Resources.WrongFileFormat, Resources.XmlFileProcessingError,
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        if (dgvCoordsTable.ColumnCount < buttonList.Count * 5)
                        {
                            dgvCoordsTable.ColumnCount += 5;
                            var buttonNumber = ((dgvCoordsTable.Columns.Count - 1) / 5).ToString();

                            dgvCoordsTable.Columns[dgvCoordsTable.Columns.Count - 5].Name = button.Attributes[0].Name + buttonNumber;
                            dgvCoordsTable.Columns[dgvCoordsTable.Columns.Count - 4].Name = button.Attributes[1].Name + buttonNumber;
                            dgvCoordsTable.Columns[dgvCoordsTable.Columns.Count - 3].Name = button.Attributes[2].Name + buttonNumber;
                            dgvCoordsTable.Columns[dgvCoordsTable.Columns.Count - 2].Name = button.Attributes[3].Name + buttonNumber;
                            dgvCoordsTable.Columns[dgvCoordsTable.Columns.Count - 1].Name = button.Attributes[4].Name + buttonNumber;

                            dgvCoordsTable.Columns[dgvCoordsTable.Columns.Count - 5].SortMode = DataGridViewColumnSortMode.NotSortable;
                            dgvCoordsTable.Columns[dgvCoordsTable.Columns.Count - 4].SortMode = DataGridViewColumnSortMode.NotSortable;
                            dgvCoordsTable.Columns[dgvCoordsTable.Columns.Count - 3].SortMode = DataGridViewColumnSortMode.NotSortable;
                            dgvCoordsTable.Columns[dgvCoordsTable.Columns.Count - 2].SortMode = DataGridViewColumnSortMode.NotSortable;
                            dgvCoordsTable.Columns[dgvCoordsTable.Columns.Count - 1].SortMode = DataGridViewColumnSortMode.NotSortable;
                        }

                        rowValues.Add(int.Parse(button.Attributes[0].Value));
                        rowValues.Add(int.Parse(button.Attributes[1].Value));
                        rowValues.Add(int.Parse(button.Attributes[2].Value));
                        rowValues.Add(int.Parse(button.Attributes[3].Value));
                        rowValues.Add(bool.Parse(button.Attributes[4].Value));
                    }

                    dgvCoordsTable.Rows.Add(rowValues.ToArray());
                }
            }
            else
            {
                // Для открытия файла используется Excel, из которого и берутся данные.
                // К сожалению, не нашёл, как брать значение не из Cells.
                // Вообще ужасно неудобная штука этот Excel!
                var excelApp = new Excel.Application();
                excelApp.Application.Workbooks.Open(dlgOpenFile.FileName);

                var excelRows = 0;
                while (Convert.ToString(((Excel.Range) excelApp.Cells[excelRows + 1, 1]).Value) != null)
                    excelRows++;

                var excelColumns = 0;
                string columnName;
                while ((columnName = Convert.ToString(((Excel.Range) excelApp.Cells[1, excelColumns + 1]).Value)) != null)
                {
                    dgvCoordsTable.ColumnCount += 1;
                    dgvCoordsTable.Columns[excelColumns].Name = columnName;
                    dgvCoordsTable.Columns[excelColumns++].SortMode = DataGridViewColumnSortMode.NotSortable;
                }

                for (var i = 2; i < (excelRows - 1); i++)
                {
                    var cells = new List<object> { Convert.ToBoolean(((Excel.Range) excelApp.Cells[i, 1]).Value) };

                    for (var j = 2 ; ((Excel.Range) excelApp.Cells[i, j]).Value != null; j += 5)
                    {
                        cells.Add(Convert.ToInt32(((Excel.Range) excelApp.Cells[i, j]).Value));
                        cells.Add(Convert.ToInt32(((Excel.Range) excelApp.Cells[i, j + 1]).Value));
                        cells.Add(Convert.ToInt32(((Excel.Range) excelApp.Cells[i, j + 2]).Value));
                        cells.Add(Convert.ToInt32(((Excel.Range) excelApp.Cells[i, j + 3]).Value));
                        cells.Add(Convert.ToBoolean(((Excel.Range) excelApp.Cells[i, j + 4]).Value));
                    }
                    

                    dgvCoordsTable.Rows.Add(cells.ToArray());
                }

                excelApp.Quit();
            }

            if (!dgvCoordsTable.Visible) dgvCoordsTable.Visible = true;
            if (!btnSaveFile.Enabled) btnSaveFile.Enabled = true;
            tsStatusLabel.Text = Resources.Done;
            Cursor.Current = Cursors.Default;
        }

        // Для экспорта в файл также используется Excel.
        private void btnSaveFile_Click(object sender, EventArgs e)
        {
            if (dlgSaveFile.ShowDialog() != DialogResult.OK)
                return;

            tsStatusLabel.Text = Resources.SavingFile;
            Cursor.Current = Cursors.WaitCursor;
            if (_isWorking) btnStartStop.PerformClick();

            if (Path.GetExtension(dlgSaveFile.FileName) == ".xml")
            {
                var xmlFile = new XmlDocument();

                var xmlDeclaration = xmlFile.CreateXmlDeclaration("1.0", "UTF-8", null);
                var root = xmlFile.DocumentElement;
                xmlFile.InsertBefore(xmlDeclaration, root);

                var mainElement = xmlFile.CreateElement("Lab1");
                xmlFile.AppendChild(mainElement);

                var framesElement = xmlFile.CreateElement("Frames");
                mainElement.AppendChild(framesElement);

                for (var i = 0; i < dgvCoordsTable.Rows.Count; i++)
                {
                    var frameElement = xmlFile.CreateElement("Frame");
                    frameElement.SetAttribute("IntersectFlag", dgvCoordsTable.Rows[i].Cells[0].Value.ToString());
                    framesElement.AppendChild(frameElement);

                    var buttonsElement = xmlFile.CreateElement("Buttons");
                    frameElement.AppendChild(buttonsElement);

                    for (var j = 1; j < dgvCoordsTable.Columns.Count; j += 5)
                    {
                        if (dgvCoordsTable.Rows[i].Cells[j].Value == null) break;

                        var buttonElement = xmlFile.CreateElement("Button");
                        buttonElement.SetAttribute("x", dgvCoordsTable.Rows[i].Cells[j].Value.ToString());
                        buttonElement.SetAttribute("y", dgvCoordsTable.Rows[i].Cells[j + 1].Value.ToString());
                        buttonElement.SetAttribute("Vx", dgvCoordsTable.Rows[i].Cells[j + 2].Value.ToString());
                        buttonElement.SetAttribute("Vy", dgvCoordsTable.Rows[i].Cells[j + 3].Value.ToString());
                        buttonElement.SetAttribute("Intersects", dgvCoordsTable.Rows[i].Cells[j + 4].Value.ToString());
                        buttonsElement.AppendChild(buttonElement);
                    }
                }

                try
                {
                    xmlFile.Save(dlgSaveFile.FileName);
                    tsStatusLabel.Text = Resources.FileSaved;
                }
                catch (Exception error)
                {
                    MessageBox.Show($"{Resources.CouldNotSaveFile}! {Resources.TryLaterPlease}\n\n:({error.Message})", Resources.FileSavingError, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tsStatusLabel.Text = Resources.CouldNotSaveFile;
                }
            }
            else
            {
                var excelApp = new Excel.Application { SheetsInNewWorkbook = 1 };
                excelApp.Application.Workbooks.Add();

                for (var i = 0; i < dgvCoordsTable.Columns.Count; i++)
                    excelApp.Cells[1, i + 1] = dgvCoordsTable.Columns[i].Name;

                for (var i = 0; i < dgvCoordsTable.Rows.Count; i++)
                {
                    for (var j = 0; j < dgvCoordsTable.Rows[i].Cells.Count; j++)
                    {
                        if (dgvCoordsTable.Rows[i].Cells[j].Value == null) break;
                        excelApp.Cells[i + 2, j + 1] = dgvCoordsTable.Rows[i].Cells[j].Value;
                    }
                }

                try
                {
                    excelApp.DisplayAlerts = false;
                    excelApp.ActiveWorkbook.SaveAs(dlgSaveFile.FileName, Excel.XlFileFormat.xlExcel8, Type.Missing, Type.Missing, false, Type.Missing, Excel.XlSaveAsAccessMode.xlShared);
                    tsStatusLabel.Text = Resources.FileSaved;
                }
                catch (COMException error)
                {
                    if (error.ErrorCode == (int) Convert.ToInt64(0x800A03EC))
                        MessageBox.Show(Resources.InvalidFilename, Resources.FileSavingError, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                        MessageBox.Show($"{Resources.CouldNotSaveFile}! {Resources.TryLaterPlease}\n\n({error.ErrorCode}; {error.Message})", Resources.FileSavingError, MessageBoxButtons.OK, MessageBoxIcon.Error);

                    tsStatusLabel.Text = Resources.CouldNotSaveFile;
                }

                excelApp.Quit();
            }

            Cursor.Current = Cursors.Default;
        }

        // Выравнивает по центру рабочей зоны тексты на обеих вкладках.
        private void AlignBlankLabels()
        {
            lblDynamicAboutBlank.Left = lblDynamicAboutBlank.Parent.Width / 2 - lblDynamicAboutBlank.Width / 2;
            lblDynamicAboutBlank.Top = lblDynamicAboutBlank.Parent.Height / 2 - lblDynamicAboutBlank.Height / 2;
            lblCoordsTableAboutBlank.Left = lblCoordsTableAboutBlank.Parent.Width / 2 - lblCoordsTableAboutBlank.Width / 2;
            lblCoordsTableAboutBlank.Top = lblCoordsTableAboutBlank.Parent.Height / 2 - lblCoordsTableAboutBlank.Height / 2;
        }

        private void Lab1Form_Shown(object sender, EventArgs e)
        {
            AlignBlankLabels();
        }

        private void tcTabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            AlignBlankLabels();
        }

        private void clearTheTableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Если программа работает, то сначала останавливаем её, иначе сразу переходим к очистке таблицы.
            if (_isWorking)
            {
                _clearTableFlag = true;
                btnStartStop.PerformClick();
            }
            else
                ClearTheDatagrid();
        }

        private void ClearTheDatagrid()
        {
            dgvCoordsTable.Rows.Clear();
            dgvCoordsTable.Columns.Clear();
            dgvCoordsTable.Visible = false;
            lblCoordsTableAboutBlank.Visible = true;
            lblDynamicAboutBlank.Visible = true;
            btnSaveFile.Enabled = false;
            tcTabs.SelectedIndexChanged += tcTabs_SelectedIndexChanged;
        }

    }
}